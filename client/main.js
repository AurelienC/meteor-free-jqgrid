import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import jqGrid from 'free-jqgrid';

import './main.html';

Template.hello.onRendered(() => {
  const table = $('#grid');
  console.log(table);
  console.log(jqGrid);
  table.jqGrid();
});

Template.hello.helpers({

});

Template.hello.events({

});
